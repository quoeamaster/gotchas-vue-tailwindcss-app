import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

// setup font-awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
// icons required
import { faChartSimple, faUserGroup, faSuitcase, faMagnifyingGlass, faEarthAmericas, } from '@fortawesome/free-solid-svg-icons'

// add to icons library 
library.add(faChartSimple, faUserGroup, faSuitcase, faMagnifyingGlass, faEarthAmericas,)

createApp(App).component('fa', FontAwesomeIcon).mount('#app')
