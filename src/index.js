// component build file entry... (not related to the direct app development)

import * as components from './components'
// import any css...
import './style.css'

const defaultComponents = components?.default
// plugin-able
const pubComponents = {
  install(Vue) {
    // register the components into the Vue object / app; hence can use it like a normal "tag"
    Object.keys(defaultComponents).forEach(name => {
      Vue.component(name, defaultComponents[name])
    })
  }
}
export default pubComponents

// tree shaking; only need a component instead of a bunch of them (dedicated)
export { TFrame } from './components/frame'
export { TCaptionLabel } from './components/captionLabel'
