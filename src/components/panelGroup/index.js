
import TParentPanel from './parentPanel.vue'
import TPanelInfo from './panelInfo.vue'
import TPanelChart from './panelChart.vue'

export { TParentPanel, TPanelInfo, TPanelChart, }
export default { TParentPanel, TPanelInfo, TPanelChart, }