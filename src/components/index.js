// for dev imports (not component building entry point usage)
import { TFrame } from './frame'
import { TCaptionLabel } from './captionLabel'
import { TParentPanel, TPanelInfo, TPanelChart, } from './panelGroup'
import { TTableInfo } from './table'

export { TFrame, TCaptionLabel, TParentPanel, TPanelInfo, TPanelChart, TTableInfo, }

export default {
  TFrame,
  TCaptionLabel,
  TParentPanel,
  TPanelInfo,
  TPanelChart,
  TTableInfo,
}

